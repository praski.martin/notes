    <div>
        <section>
            <div class="message">
                <?php
                if (!empty($params['error'])){
                    switch ($params['error']){
                        case 'missingNoteId':
                            echo 'Niepoprawny identyfikator notatki';
                            break;
                        case 'noteNotFound':
                            echo 'Notatka nie została znaleziona';
                            break;
                    }
                }
                ?>
            </div>
            <div class="message">
                <?php
                if (!empty($params['before'])){
                    switch ($params['before']){
                        case 'created':
                            echo 'Notatka została utworzona';
                            break;
                        case 'deleted':
                            echo 'Notatka została usunięta';
                            break;
                        case 'edited':
                            echo 'Notatka zostałą zaktualizowana';
                            break;
                    }
                }
                ?>
            </div>
            <?php
            $sort = $params['sort'] ?? [];
            $by = $sort['by'] ?? 'title';
            $order = $sort['order'] ?? 'desc';

            $page = $params['page'] ?? [];
            $size = $page['size'] ?? 10;
            $currentPage = $page['number'] ?? 1;
            $pages = $page['pages'] ?? 1;

            $phrase = $params['phrase'] ?? null;
            ?>
            <div>
                <form action="/notes/" method="GET">
                    <div>
                        <label>Wyszukaj :<input type="text" name="phrase" value="<?php echo $phrase ?>"></label>
                    </div>
                    <div>
                        <div>Sortuj po:</div>
                        <label>Tytule: <input name="sortby" type="radio" value="title"<?php echo $by === 'title'
                        ? 'checked' : ''?>/></label>
                        <label>Dacie: <input name="sortby" type="radio" value="created"<?php echo $by === 'created'
                        ? 'checked' : ''?>/></label>
                    </div>
                    <div>
                        <div>Kierunek sortowania:</div>
                        <label>Rosnąco: <input name="sortorder" type="radio" value="asc"<?php echo $order === 'asc'
                        ? 'checked' : ''?>/></label>
                        <label>Malejąco: <input name="sortorder" type="radio" value="desc"<?php echo $order === 'desc'
                        ? 'checked' : ''?>/></label>
                    </div>
                    <div>
                        <div>Rozmiar paczki</div>
                        <label>1<input name="pagesize" type="radio" value="1"<?php echo $size ===1 ? 'checked' :'' ?>></label>
                        <label>5<input name="pagesize" type="radio" value="5"<?php echo $size ===5 ? 'checked' :'' ?>></label>
                        <label>10<input name="pagesize" type="radio" value="10"<?php echo $size ===10 ? 'checked' :'' ?>></label>
                        <label>25<input name="pagesize" type="radio" value="25"<?php echo $size ===25 ? 'checked' :'' ?>></label>
                    </div>
                    <button type="submit" value="Wyślij" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Tytuł</th>
                            <th>Data</th>
                            <th>Opcje</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($params['notes'] ?? [] as $note): ?>
                            <tr>
                                <td><?php echo $note['id'] ?> </td>
                                <td><?php echo $note['title'] ?> </td>
                                <td><?php echo $note['created'] ?> </td>
                                <td>
                                    <a href="/notes?action=show&id=<?php echo $note['id'] ?>">
                                        <button type="button" class="btn btn-outline-info">Szczegóły</button>
                                    </a>
                                    <a href="/notes?action=delete&id=<?php echo $note['id'] ?>">
                                        <button type="button" class="btn btn-outline-danger">Usuń</button>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <?php
                $paginationUrl = "&phrase=$phrase&pagesize=$size?sortby=$by&sortorder=$order";
            ?>
            <nav aria-label="Page navigation example ">
                <ul class="pagination justify-content-center">
                    <?php if ($currentPage !== 1): ?>
                    <li class="page-item">
                        <a class="page-link" href="/notes/?page=<?php echo $currentPage - 1 . $paginationUrl ?>">
                            <button>Prev</button>
                        </a>
                    </li>
                    <?php endif; ?>
                    <?php for($i = 1; $i <= $pages; $i++): ?>
                    <li class="page-item">
                        <a class="page-link active"  href="/notes/?page=<?php echo $i . $paginationUrl ?>">
                            <button><?php echo $i; ?></button>
                        </a>
                    </li>
                    <?php endfor; ?>
                    <?php if ($currentPage < $pages): ?>
                    <li class="page-item">
                        <a class="page-link" href="/notes/?page=<?php echo $currentPage + 1 . $paginationUrl ?>">
                            <button>Next</button>
                        </a>
                    </li>
                    <?php endif; ?>
                </ul>
            </nav>
        </section>
    </div>

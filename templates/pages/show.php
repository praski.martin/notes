<div>
    <?php $note = $params['note'] ?? null; ?>
    <?php if($note): ?>
        <ul>
            <li>Id:<?php echo $note['id'] ?></li>
            <li>Tytuł:<?php echo $note['title'] ?></li>
            <li><?php echo $note['description'] ?></li>
            <li>Zapisano:<?php echo $note['created'] ?></li>
        </ul>
        <a href="/notes/?action=edit&id=<?php echo $note['id'] ?>">
            <button type="button" class="btn btn-info">Edytuj</button>
        </a>
    <?php else: ?>
        <div>Brak notatki do wyświetlenia</div>
    <?php endif; ?>
        <a href="/notes/">
            <button type="button" class="btn btn-info">Powrót do listy notatek</button>
        </a>
</div>
<html>
<head>
    <title>Notes</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>
<div class="jumbotron text-center" style="margin-bottom:0">
    <h1>Moje notatki</h1>
</div>
<div class="container" style="margin-top:30px">
    <div class="row">
        <div class="col-sm-4">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="/notes">Lista notatek</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/notes?action=create">Nowa notatka</a>
                </li>
            </ul>
        </div>
        <div class="col-sm-8">
            <?php
            require_once("templates/pages/$page.php");
            ?>
        </div>
    </div>
</div>
<div class="jumbotron text-center" style="margin-bottom:0">NOTATKI - projekt w kursie PHP</div>
</body>
</html>
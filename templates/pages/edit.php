<div>
    <h3>edycja notatki</h3>
    <div>
        <?php if (!empty($params['note'])): ?>
        <?php $note = $params['note']; ?>
        <form action="/notes/?action=edit" method="post">
            <input name="id" type="hidden" value="<?php echo $note['id'] ?>"/>
            <div class="form-group">
                <label>Tytuł<span class="required">*</span></label>
                <input class="form-control" type="text"  name="title" value="<?php echo $note['title'] ?>">
            </div>
            <div class="form-group">
                <label>Treść</label>
                <textarea class="form-control" name="description"><?php echo $note['description'] ?></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Dokonaj zmian</button>
        </form>
        <?php else: ?>
        <div>
            Brak danych do wyświetlenia
            <a href="/notes/">
                <button >Powrót do listy notatek</button>
            </a>
        </div>
        <?php endif; ?>
    </div>
</div>